#!/usr/bin/env bash

set -o errexit -o nounset -o pipefail

[ ! -d test ] && mkdir -p test

[ ! -f test/ca ] && ssh-keygen -q -t ed25519 -f test/ca -N ''
[ ! -f test/ca2 ] && ssh-keygen -q -t rsa -b 4096 -f test/ca2 -N ''
[ ! -f test/id_ed25519 ] && ssh-keygen -q -t ed25519 -f test/id_ed25519 -N ''
[ ! -f test/id_ed25519_usable ] && ssh-keygen -q -t ed25519 -f test/id_ed25519_usable -N ''
[ ! -f test/id_rsa ] && ssh-keygen -q -t rsa -b 4096 -f test/id_rsa -N ''

for i in test/krl-test test/krl-test-unsigned; do
	ssh-keygen -k -f "$i" -s test/ca.pub <(echo serial: 1)
	ssh-keygen -k -u -f "$i" -s test/ca.pub <(echo serial: 2)
	ssh-keygen -k -u -f "$i" -s test/ca.pub <(echo serial: 13)
	ssh-keygen -k -u -f "$i" -s test/ca.pub <(echo serial: 1338)
	ssh-keygen -k -u -f "$i" -s test/ca.pub <(echo serial: 500)
	ssh-keygen -k -u -f "$i" -s test/ca2.pub <(echo serial: 500)
	ssh-keygen -k -u -f "$i" -s test/ca.pub <(echo serial: 815-1337)
	ssh-keygen -k -u -f "$i" -s test/ca.pub <(echo id: foobar)
	ssh-keygen -k -u -f "$i" -s test/ca.pub test/id_ed25519.pub
	ssh-keygen -s test/ca -I "testidentity ed25519" -n foobar -V +12h test/id_ed25519.pub
	ssh-keygen -s test/ca -I "testidentity ed25519 usable" -n foobar -V +12h test/id_ed25519_usable.pub
	ssh-keygen -k -u -f "$i" test/id_ed25519-cert.pub
	ssh-keygen -k -u -f "$i" -s test/ca.pub test/id_rsa.pub
	ssh-keygen -s test/ca -I "testidentity rsa" -n foobar -V +12h test/id_rsa.pub
	ssh-keygen -k -u -f "$i" test/id_rsa-cert.pub
	ssh-keygen -k -u -f "$i" <(echo sha1: $(cat test/id_ed25519.pub))
	ssh-keygen -k -u -f "$i" <(echo sha1: $(cat test/id_rsa.pub))
	ssh-keygen -k -u -f "$i" <(echo sha256: $(cat test/id_ed25519.pub))
	ssh-keygen -k -u -f "$i" <(echo sha256: $(cat test/id_rsa.pub))

	#ssh-keygen -k -u -f "$i" <(echo hash: $(ssh-keygen -lf test/id_ed25519.pub | awk '{print $2}'))
done

[ ! -f test/ssh_host_key_ed25519 ] && ssh-keygen -q -t ed25519 -f test/ssh_host_key_ed25519 -N ''
[ ! -f test/ssh_host_key_rsa ] && ssh-keygen -q -t rsa -b 4096 -f test/ssh_host_key_rsa -N ''

[ ! -f test/ssh_host_key_ed25519-cert.pub ] && ssh-keygen -s test/ca2 -V +12h -I "localhost test host cert ed25519" -n "localhost,::1,127.0.0.1" -h test/ssh_host_key_ed25519.pub
[ ! -f test/ssh_host_key_rsa-cert.pub ] &&  ssh-keygen -s test/ca -V +12h -I "localhost test host cert rsa" -n "localhost,::1,127.0.0.1" -h test/ssh_host_key_rsa.pub

cat <<EOF > test/sshd_config
ListenAddress ::1
ListenAddress 127.0.0.1
Port 2222
HostKey $(pwd)/test/ssh_host_key_ed25519
HostKey $(pwd)/test/ssh_host_key_rsa
HostCertificate $(pwd)/test/ssh_host_key_ed25519-cert.pub
HostCertificate $(pwd)/test/ssh_host_key_rsa-cert.pub
TrustedUserCAKeys $(pwd)/test/ca.pub
TrustedUserCAKeys $(pwd)/test/ca2.pub
AuthorizedPrincipalsFile $(pwd)/test/authorized_principals
RevokedKeys $(pwd)/test/krl-test
PasswordAuthentication no
EOF

cat <<EOF > test/authorized_principals
foobar
EOF

echo "@cert-authority * $(cat test/ca.pub)" > test/client_known_hosts
echo "@cert-authority * $(cat test/ca2.pub)" >> test/client_known_hosts
