# `ssh-krl`

`ssh-krl` allows inspection and modification of ssh pki key revocation lists (KRL) in ways the
original openssh toolchain currently (version 8.5) does not. In particular, detailed listing
of revoked certificate key ids, serials, public keys and fingerprint is possible. Signing and
verifying KRL is also possible.

Note that a single KRL may contain revoked certificates of one or more certificate
authorities. The pubkey of the corresponding CA is then embedded into the KRL. This means that
a KRL may also be signed by one or multiple CAs. It is up to the verifier to determine the
meaning of present or absent signatures. None of the openssh-tooling (neither `ssh-keygen` nor
the openssh-server itself) currently verifies signatures of KRL. Possibly because of this
fact. One option is to verify a KRL before deploying it to the machines. This is where
`ssh-krl verify` comes into play. Given no arguments except a KRL file e.g. `ssh-krl verify
revoked_keys`, we check that each of the CA for which the KRL contains certificate
revocations, a signature exists. `ssh-krl verify` may also take a list of pubkeys for which a
signature must exist e.g. `ssh-krl verify revoked_keys ca.pub ca2.pub`.

# build

    go build

# usage

```
Usage of ./ssh-krl:
The following commands are available:
  view KRL...
	Print the contents of one or more KRL
  sign KRL PRIVKEYFILE...
	Sign a KRL with one or more Privkeys. All previous signatures will be removed.
  verify KRL [PUBKEYFILE...]
	Verify the signatures of a KRL against one or multiple pubkeys. If no pubkey is given, the verification only suceeds signatures of all embedded CA pubkeys are present. Embedded CA pubkeys are only present if certificates have been revoked.
```

# How to use openssh to create and update KRLs

The `ssh-keygen` binary is able to create, update (append to), and query KRLs. Querying is
done via `ssh-keygen -Q` and given a pubkey or ssh cert, returns the revocation status via
exit code and verbose output:

    $ ssh-keygen -Q -f revoked_keys id_ed25519.pub id_rsa.pub
    id_ed25519.pub (user@host): REVOKED
    id_rsa.pub (user@host): OK
    $ echo $?
    1

# How to use `ssh-krl` to view, sign, and verify KRLs

`ssh-krl` produces much more verbose output upon querying:

```
$ ./generate-krl.sh
[...]
$ ./ssh-krl view test/krl-test
Version 0
KRL issued on 2021-03-14 01:10:17 +0100 CET
Revoked certificates from CA SHA256:CU7b/8O+xl22ut2lcdVcp08umfzcavJInSLBYBbKPyU
	CertificateSerialBitmap: Offset 1 Bitmap 4099
	CertificateSerialList: 500
	CertificateSerialRange: 815 to 1338
	CertificateKeyID: foobar
	CertificateKeyID: testidentity ed25519
	CertificateKeyID: testidentity rsa
Revoked certificates from CA SHA256:xAGzXGKwhI+pw29Ngs+fDp6ote7h+qQLE1pFmogfpzc
	CertificateSerialList: 500
Revoked public keys:
	ssh-rsa SHA256:W/MknFvRFnx8EOHZH6UeVPmoQjnyAAA5491As780EcE
	ssh-ed25519 SHA256:6ocHXoiyngxbilqTEcphJTjGjADKOq79oSLvbZwFw7s
Revoked SHA1 fingerprints:
	Fingerprint: 16:02:9f:f1:e4:0c:83:f4:22:89:be:11:1d:c2:a8:99
	Fingerprint: 39:bb:79:12:b0:27:2a:89:c8:93:56:ff:9f:8c:f3:f0
Revoked SHA256 fingerprints:
	Fingerprint: SHA256:W/MknFvRFnx8EOHZH6UeVPmoQjnyAAA5491As780EcE
	Fingerprint: SHA256:6ocHXoiyngxbilqTEcphJTjGjADKOq79oSLvbZwFw7s
This KRL is *not* signed.
```

Sign a KRL:

```
$ ./ssh-krl sign test/krl-test test/ca test/ca2
$ ./ssh-krl verify test/krl-test
$ ./ssh-krl view test/krl-test
Version 1615730240
KRL issued on 2021-03-14 14:57:20 +0100 CET
Revoked certificates from CA SHA256:jP+6oiCJL/fiwKRy1gF0qucc4QWUIwdxZ1L+kTyat+I
	CertificateSerialBitmap: Offset 1 Bitmap 4099
	CertificateSerialList: 500
	CertificateSerialRange: 815 to 1338
	CertificateKeyID: foobar
	CertificateKeyID: testidentity ed25519
	CertificateKeyID: testidentity rsa
Revoked certificates from CA SHA256:uXcpQ+NCK4PSyaGqncnwWvBpzQfJ6JDWPkVa7NQRs+Q
	CertificateSerialList: 500
Revoked public keys:
	ssh-rsa SHA256:2g0kr2/RyBbetsCmR7cM+zziinRBSNIwAF8XOLX/7ys
	ssh-ed25519 SHA256:iA4Q56CGyk3Oaq5ynVaWrRVoLgKyBCOv0V8ooiz23ao
Revoked SHA1 fingerprints:
	Fingerprint: 43:65:53:60:68:5e:6e:10:c9:6c:21:1b:77:a2:b7:ce
	Fingerprint: 71:79:63:91:15:1a:53:6c:1d:8d:6f:86:9d:05:34:1e
Revoked SHA256 fingerprints:
	Fingerprint: SHA256:iA4Q56CGyk3Oaq5ynVaWrRVoLgKyBCOv0V8ooiz23ao
	Fingerprint: SHA256:2g0kr2/RyBbetsCmR7cM+zziinRBSNIwAF8XOLX/7ys
This KRL is signed by the following key(s):
	Fingerprint: SHA256:jP+6oiCJL/fiwKRy1gF0qucc4QWUIwdxZ1L+kTyat+I (de:28:ba:18:7e:eb:92:09:06:2c:8e:f1:0e:af:f8:dd)
	Fingerprint: SHA256:uXcpQ+NCK4PSyaGqncnwWvBpzQfJ6JDWPkVa7NQRs+Q (42:74:c7:e4:c6:89:60:9a:b9:be:19:56:7d:fd:16:c8)
```
