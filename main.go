package main

import (
	"crypto/rand"
	"encoding/base64"
	"fmt"
	k "github.com/porkbeans/krl"
	"golang.org/x/crypto/ssh"
	"golang.org/x/crypto/ssh/terminal"
	"io/ioutil"
	"log"
	"os"
	"strings"
	"syscall"
	"time"
)

func printKRL(krl *k.KRL) {
	fmt.Printf("Version %d\n", krl.Version)
	fmt.Printf("KRL issued on %s\n", time.Unix(int64(krl.GeneratedDate), 0))
	if krl.Comment != "" {
		fmt.Printf("Comment: %s\n", krl.Comment)
	}
	for _, section := range krl.Sections {
		switch v := section.(type) {
		case *k.KRLCertificateSection:
			fmt.Printf("Revoked certificates from CA %s %s\n", v.CA.Type(), ssh.FingerprintSHA256(v.CA))
			for _, subsection := range v.Sections {
				switch t := subsection.(type) {
				case *k.KRLCertificateSerialList:
					for _, item := range *t {
						fmt.Printf("\tCertificateSerialList: %d\n", item)
					}
				case *k.KRLCertificateSerialRange:
					fmt.Printf("\tCertificateSerialRange: %d to %d\n", t.Min, t.Max)
				case *k.KRLCertificateSerialBitmap:
					fmt.Printf("\tCertificateSerialBitmap: Offset %d Bitmap %v\n", t.Offset, t.Bitmap)
				case *k.KRLCertificateKeyID:
					for _, keyid := range *t {
						fmt.Printf("\tCertificateKeyID: %s\n", keyid)
					}
				}
			}
		case *k.KRLExplicitKeySection:
			fmt.Printf("Revoked public keys:\n")
			for _, pubkey := range *v {
				fmt.Printf("\t%s %s (%s)\n", pubkey.Type(), ssh.FingerprintSHA256(pubkey), ssh.FingerprintLegacyMD5(pubkey))
			}
		case *k.KRLFingerprintSection:
			fmt.Printf("Revoked SHA1 fingerprints:\n")
			for _, hash := range *v {
				fmt.Printf("\tSHA1:%s\n", strings.TrimRight(base64.StdEncoding.EncodeToString(hash[:]), "="))

				// this is the md5 code
				// fmt.Printf("\tFingerprint: ")
				// for i := 0; i < 16; i++ {
				// if i > 0 {
				// fmt.Print(":")
				// }
				// fmt.Printf("%02x", hash[i])
				// }
				// fmt.Print("\n")
			}
		case *k.KRLFingerprintSHA256Section:
			fmt.Printf("Revoked SHA256 fingerprints:\n")
			for _, hash := range *v {
				fmt.Printf("\tSHA256:%s\n", strings.TrimRight(base64.StdEncoding.EncodeToString(hash[:]), "="))
			}
		}
	}
	if len(krl.SigningKeys) > 0 {
		fmt.Printf("This KRL is signed by the following key(s):\n")
		for _, pubkey := range krl.SigningKeys {
			fmt.Printf("\t%s %s (%s)\n", pubkey.Type(), ssh.FingerprintSHA256(pubkey), ssh.FingerprintLegacyMD5(pubkey))
		}
	} else {
		fmt.Printf("This KRL is *not* signed.\n")
	}
}

func signKRL(krl *k.KRL, privkeys ...ssh.Signer) ([]byte, error) {
	newKRLBytes, err := krl.Marshal(rand.Reader, privkeys...)
	if err != nil {
		return nil, err
	}
	return newKRLBytes, nil
}

// verify gets called with a *KRL created by parseKRL. parseKRL does verify all signatures to
// see if the signature matches the pubkey of the signature. It does not check against all
// embedded CA.
func verify(krl *k.KRL, verifyargs []string) error {
	pubkeys := make([]ssh.PublicKey, 0)
	if len(verifyargs) > 0 {
		// if verify is given arguments, read the pubkeys from there
		for _, file := range verifyargs {
			content, err := ioutil.ReadFile(file)
			if err != nil {
				log.Fatalf("could not read file %s", file)
			}
			pubkey, _, _, _, err := ssh.ParseAuthorizedKey(content)
			if err != nil {
				log.Fatalf("could not parse pubkey %s", file)
			}
			pubkeys = append(pubkeys, pubkey)
		}
	} else {
		// if verify is not given arguments, grab all pubkeys from the KRL itself
		for _, section := range krl.Sections {
			switch v := section.(type) {
			case *k.KRLCertificateSection:
				pubkeys = append(pubkeys, v.CA)
			}
		}
	}

	for _, pubkey := range pubkeys {
		pubkeysigned := false
		for _, krlsigner := range krl.SigningKeys {
			if ssh.FingerprintSHA256(pubkey) == ssh.FingerprintSHA256(krlsigner) {
				pubkeysigned = true
			}
		}
		if !pubkeysigned {
			return fmt.Errorf("The signature of pubkey %s was not found", ssh.FingerprintSHA256(pubkey))
		}
	}
	return nil
}

func getPrivkeys(privkeyargs []string) []ssh.Signer {
	privkeys := make([]ssh.Signer, 0)
	for _, file := range privkeyargs {
		content, err := ioutil.ReadFile(file)
		if err != nil {
			log.Fatal(err)
		}
		privkey, err := ssh.ParsePrivateKey(content)
		if err == nil {
			privkeys = append(privkeys, privkey)
			continue
		}

		// err is set, check for missing passphrase
		if _, ok := err.(*ssh.PassphraseMissingError); !ok {
			log.Fatal(err)
		}

		// this is a missing passphrase error
		for i := 0; i < 3; i++ {
			fmt.Fprintf(os.Stderr, "Enter passphrase for %s: ", file)
			bytePassword, err := terminal.ReadPassword(int(syscall.Stdin))
			fmt.Fprintf(os.Stderr, "\n")
			if err != nil {
				log.Fatal(err)
			}
			privkey, err = ssh.ParsePrivateKeyWithPassphrase(content, bytePassword)
			if err == nil {
				privkeys = append(privkeys, privkey)
				break
			}
		}
	}
	return privkeys
}

func sign(krl *k.KRL, krlfile string, privkeyargs []string) {
	if len(privkeyargs) == 0 {
		log.Fatalf("No private key given")
	}

	privkeys := getPrivkeys(privkeyargs)

	if len(privkeys) != len(privkeyargs) {
		log.Fatalf("Not all privkeys have been decrypted")
	}

	raw, err := signKRL(krl, privkeys...)
	if err != nil {
		log.Fatal(err)
	}
	krl, err = k.ParseKRL(raw)
	if err != nil {
		log.Fatal(err)
	}
	err = ioutil.WriteFile(krlfile, raw, 0)
	if err != nil {
		log.Fatalf("Error writing to file %s: %v", krlfile, err)
	}
}

func parseKRL(filename string) *k.KRL {
	content, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatal(err)
	}
	krl, err := k.ParseKRL(content)
	if err != nil {
		log.Fatal(err)
	}
	return krl
}

func merge(dest string, krls []string) {
	krl := &k.KRL{}
	for _, single := range krls {
		s := parseKRL(single)
		for _, section := range s.Sections {
			// TODO we're appending blindly. Duplicate sections are the result
			// ssh-keygen -Q seems to handle duplicates and we do, too.
			// ssk-keygen -k -u -f krl key will fix up and deduplicate sections
			// and entries
			krl.Sections = append(krl.Sections, section)
		}
	}
	raw, err := krl.Marshal(rand.Reader)
	if err != nil {
		log.Fatalf("Could not marshal KRL: %v", err)
	}
	err = ioutil.WriteFile(dest, raw, 0644)
	if err != nil {
		log.Fatalf("Error writing to file %s: %v", dest, err)
	}
}

func usage() {
	fmt.Fprintf(os.Stderr, "Usage of %s:\n", os.Args[0])
	fmt.Fprint(os.Stderr, "The following commands are available:\n")
	fmt.Fprint(os.Stderr, "  view KRL...\n")
	fmt.Fprint(os.Stderr, "\tPrint the contents of one or more KRL\n")
	fmt.Fprint(os.Stderr, "  verify KRL [PUBKEYFILE...]\n")
	fmt.Fprint(os.Stderr, "\tVerify the signatures of a KRL against one or multiple pubkeys. If no pubkey is given, the verification only suceeds signatures of all embedded CA pubkeys are present. Embedded CA pubkeys are only present if certificates have been revoked.\n")
	fmt.Fprint(os.Stderr, "  sign KRL PRIVKEYFILE...\n")
	fmt.Fprint(os.Stderr, "\tSign a KRL with one or more Privkeys. All previous signatures will be removed.\n")
	fmt.Fprint(os.Stderr, "  merge NEWKRL KRL KRL...\n")
	fmt.Fprint(os.Stderr, "\tMerge two or more KRL into a new one.\n")
	fmt.Fprint(os.Stderr, "\tAttention: The merge implementation is rudimentary and simply concatenates sections without deduplication.\n")
	os.Exit(1)
}

func main() {
	if len(os.Args) <= 1 {
		usage()
	}
	command := os.Args[1]
	args := os.Args[2:]

	switch command {
	case "show":
		fallthrough
	case "view":
		if len(args) < 1 {
			usage()
		}
		for _, krlfile := range args {
			printKRL(parseKRL(krlfile))
		}
	case "sign":
		if len(args) < 2 {
			usage()
		}
		krlfile := args[0]
		krl := parseKRL(krlfile)
		sign(krl, krlfile, args[1:])
	case "verify":
		if len(args) < 1 {
			usage()
		}
		krlfile := args[0]
		krl := parseKRL(krlfile)
		err := verify(krl, args[1:])
		if err != nil {
			log.Fatal(err)
		}
	case "merge":
		if len(args) < 3 {
			usage()
		}
		dest := args[0]
		merge(dest, args[1:])
	default:
		usage()
	}
}
