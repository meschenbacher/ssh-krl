#!/usr/bin/env bash

set -o errexit -o nounset -o pipefail

/usr/sbin/sshd -f test/sshd_config -d &
ssh -p 2222 $(whoami)@localhost -o PasswordAuthentication=no -o UserKnownHostsFile=test/client_known_hosts -i test/id_rsa exit && exit 1

/usr/sbin/sshd -f test/sshd_config -d &
ssh -p 2222 $(whoami)@localhost -o PasswordAuthentication=no -o UserKnownHostsFile=test/client_known_hosts -i test/id_ed25519 exit && exit 1

/usr/sbin/sshd -f test/sshd_config -d &
ssh -p 2222 $(whoami)@localhost -o PasswordAuthentication=no -o UserKnownHostsFile=test/client_known_hosts -i test/id_ed25519_usable exit
