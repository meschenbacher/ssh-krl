.PHONY: all test clean

all: ssh-krl

ssh-krl: main.go
	go build

test: ssh-krl
	./generate-krl.sh
	./test-krl.sh
	./ssh-krl view test/krl-test
	./ssh-krl sign test/krl-test test/ca test/ca2
	./ssh-krl view test/krl-test
	./ssh-krl verify test/krl-test
	./ssh-krl verify test/krl-test-unsigned || true

clean:
	rm -f ssh-krl
	rm -rf test
