module gitlab.com/meschenbacher/ssh-krl

go 1.15

require (
	github.com/porkbeans/krl v0.0.0-20190120140803-7569b7c3179a
	golang.org/x/crypto v0.0.0-20210220033148-5ea612d1eb83
)
